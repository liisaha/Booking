﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class TrainingType
    {

        public int Id { get; set; }
        [Display(Name = "Treeningu nimi")]
        [Required(ErrorMessage = "Nimi ei või olla tühi.")]
        public string Name { get; set; }
        [Display(Name = "Kirjeldus")]
        [Required(ErrorMessage = "Kirjeldus ei või olla tühi.")]
        public string Description { get; set; }
        [Display(Name = "Hind")]
        [Required(ErrorMessage = "Hind ei või olla tühi.")]
        public int Price { get; set; }
        [Display(Name = "Maksimaalne osalejate arv")]
        [Required(ErrorMessage = "Maksimaalne osalejate arv on määramata.")]
        public int MaxParticipants { get; set; }
        [Display(Name = "Ruum")]
        [Required(ErrorMessage = "Ruum on määramata.")]
        public string Room { get; set; }

        public ICollection<SpecialistTrainingType> SpecialistTrainingType { get; set; }
        public ICollection<Training> Training { get; set; }

    }
}