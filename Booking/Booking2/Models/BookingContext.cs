﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class BookingContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public BookingContext() : base("name=BookingContext")
        {
        }

        public System.Data.Entity.DbSet<Booking.Models.Training> Trainings { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.Specialist> Users { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.TrainingType> TrainingTypes { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.Client> Users { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.SpecialistTrainingType> SpecialistTrainingTypes { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.TrainingClient> TrainingUsers { get; set; }
    }
}
