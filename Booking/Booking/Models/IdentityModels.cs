﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Booking.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Required(ErrorMessage = "Nimi ei või olla tühi.")]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nimi ei või olla tühi.")]
        [Display(Name = "Perekonnanimi")]
        public string LastName { get; set; }

        [Display(Name = "Telefoninumber")]
        [Required(ErrorMessage = "Telefoninumber ei või olla tühi.")]
        public string Phone { get; set; }

        public ICollection<UserTrainingType> UserTrainingTypes { get; set; }
        public ICollection<TrainingUser> TrainingUsers { get; set; }
        public ICollection<Training> Trainings { get; set; }

    }



    public class BookingContext : IdentityDbContext<ApplicationUser>
    {
        public BookingContext()
            : base("BookingContext", throwIfV1Schema: false)
        {
        }
        public static BookingContext Create()
        {
            return new BookingContext();
        }
        public System.Data.Entity.DbSet<Booking.Models.Training> Trainings { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.TrainingType> TrainingTypes { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.UserTrainingType> UserTrainingTypes { get; set; }

        public System.Data.Entity.DbSet<Booking.Models.TrainingUser> TrainingUsers { get; set; }

        //public System.Data.Entity.DbSet<Booking.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}