﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Booking.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        
        [Display(Name = "E-post")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Palun sisesta korrektne e-posti aadress")]

        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        
        [Display(Name = "Kood")]
        [Required(ErrorMessage = "Kood ei või olla tühi.")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Jäta kasutaja meelde?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        
        [Display(Name = "E-post")]
        [Required(ErrorMessage = "E-posti aadress ei või olla tühi.")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        
        [Display(Name = "E-post")]
        [Required(ErrorMessage = "E-posti aadress ei või olla tühi.")]
        [EmailAddress(ErrorMessage ="E-posti formaat on vale.")]
        public string Email { get; set; }

        [Display(Name = "Parool")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Parool ei või olla tühi.")]

       
        public string Password { get; set; }

        [Display(Name = "Jäta kasutaja meelde?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Display(Name = "Eesnimi")]
        [Required(ErrorMessage = "Eesnimi ei või olla tühi.")]
        public string FirstName { get; set; }

        [Display(Name = "Perekonnanimi")]
        [Required(ErrorMessage = "Perekonnanimi ei või olla tühi.")]
        public string LastName { get; set; }

       
        [EmailAddress]
        [Display(Name = "E-post")]
        [Required(ErrorMessage = "E-posti aadress ei või olla tühi.")]
        public string Email { get; set; }

        [Display(Name = "Telefoninumber")]
        [Required(ErrorMessage = "Telefoninumber ei või olla tühi.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Parool ei või olla tühi.")]
        [Display(Name = "Parool")]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} märki pikk.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Kinnita parool")]
        [Required(ErrorMessage = "Sisestatud parool on erinev.")]

        [Compare("Password", ErrorMessage = "Sisestatud paroolid on erinevad. Palun proovi uuesti.")]
        public string ConfirmPassword { get; set; }

        // See on rolli nimi:
        public string Name { get; set; }
    }

    public class ResetPasswordViewModel
    {
        
        [EmailAddress]
        [Display(Name = "E-post")]
        [Required(ErrorMessage = "E-posti aadress ei või olla tühi.")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = " {0} peab olema vähemalt {2} märki pikk.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Parool")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita parool")]
        [Compare("Password", ErrorMessage = "Sisestatud paroolid on erinevad. Palun proovi uuesti.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
      
        [EmailAddress]
        [Display(Name = "E-post")]
        [Required(ErrorMessage = "E-posti aadress ei või olla tühi.")]
        public string Email { get; set; }
    }
}
