﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    public class CustomerTrainingsController : Controller
    {
        private BookingContext db = new BookingContext();

        // GET: CustomerTrainings
        public ActionResult Index()
        {
            var trainings = db.Trainings.Include(t => t.Specialist).Include(t => t.TrainingType);
            return View(trainings.ToList());
        }

        // GET: CustomerTrainings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        //// GET: CustomerTrainings/Create
        //public ActionResult Create()
        //{
        //    ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName");
        //    ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");
        //    return View();
        //}

        //// POST: CustomerTrainings/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Date,StartTime,Duration,SpecialistId,TrainingTypeId")] Training training)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Trainings.Add(training);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", training.SpecialistId);
        //    ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);
        //    return View(training);
        //}


        //public ActionResult Join(int id)





        // GET: CustomerTrainings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // POST: CustomerTrainings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,StartTime,Duration,SpecialistId,TrainingTypeId")] Training training)
        {
            if (ModelState.IsValid)
            {
                db.Entry(training).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", training.SpecialistId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);
            return View(training);
        }

        // GET: CustomerTrainings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // POST: CustomerTrainings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Training training = db.Trainings.Find(id);
            db.Trainings.Remove(training);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }            
    }
}
