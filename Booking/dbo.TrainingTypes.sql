﻿CREATE TABLE [dbo].[TrainingTypes] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (MAX) NOT NULL,
    [Description]     NVARCHAR (MAX) NOT NULL,
    [Price]           INT            NOT NULL,
    [MaxParticipants] INT            NOT NULL,
    [Room]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_dbo.TrainingTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

