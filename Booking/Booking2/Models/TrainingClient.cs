﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class TrainingClient
    {
        public int Id { get; set; }
        public DateTime DateAdded { get; set; }


        //Foreign key
        public int ClientId { get; set; }
        public Client Client { get; set; }

        public int TrainingId { get; set; }
        public Training Training { get; set; }






    }
}