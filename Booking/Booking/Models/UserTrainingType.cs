﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class UserTrainingType
    {
        public int Id { get; set; }

        //Foreign Key

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        //Foreign Key
        public int TrainingTypeId { get; set; }
        public TrainingType TrainingType { get; set; }

    }
}