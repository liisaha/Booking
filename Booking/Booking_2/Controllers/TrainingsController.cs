﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    public class TrainingsController : Controller
    {
        private BookingContext db = new BookingContext();

        // GET: Trainings
        public ActionResult Index()
        {
            var trainings = db.Trainings.Include(t => t.Specialist).Include(t => t.TrainingType);
            return View(trainings.ToList());
        }

        // GET: Trainings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // GET: Trainings/Create
        public ActionResult Create()
        {
            ViewBag.SpecialistId = new SelectList(db.Specialists, "Id", "FirstName");
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");
            return View();
        }

        // POST: Trainings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,StartTime,Duration,SpecialistId,TrainingTypeId")] Training training)
        {
            if (ModelState.IsValid)
            {
                ViewBag.SpecialistId = new SelectList(db.Specialists, "Id", "FirstName", training.SpecialistId);
                ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);

                var newTrainingStartDateWithTime = training.Date.AddHours(training.StartTime.Hour).AddMinutes(training.StartTime.Minute);
                var newTrainingFinishDateWithTime = newTrainingStartDateWithTime.AddMinutes(training.Duration);

                if (newTrainingStartDateWithTime > DateTime.Now)
                {
                    bool timeAlreadyBooked = false;

                    foreach (var oneTraining in db.Trainings)
                    {
                        var savedTrainingStartDateWithTime = oneTraining.Date.AddHours(oneTraining.StartTime.Hour).AddMinutes(oneTraining.StartTime.Minute);
                        var savedTrainingFinishDateWithTime = savedTrainingStartDateWithTime.AddMinutes(oneTraining.Duration);

                        if (newTrainingFinishDateWithTime > savedTrainingStartDateWithTime && newTrainingStartDateWithTime < savedTrainingFinishDateWithTime)
                        {
                            timeAlreadyBooked = true;
                            break;
                        }
                    }

                    if (!timeAlreadyBooked)
                    {
                        db.Trainings.Add(training);
                        db.SaveChanges();
                        TempData["SavedMessage"] = "Treening on lisatud";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Selleks ajaks on planeeritud teine treening. Palun vali uus treeningu kellaaeg.");
                        return View(training);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Treeningu algusaeg peab olema hiljem praegusest kuupäevast ja kellaajast.");
                    return View(training);
                }
            }
            return View(training);
        }

        // GET: Trainings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            ViewBag.SpecialistId = new SelectList(db.Specialists, "Id", "FirstName", training.SpecialistId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);
            return View(training);
        }

        // POST: Trainings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,StartTime,Duration,SpecialistId,TrainingTypeId")] Training training)
        {
            if (ModelState.IsValid)
            {

                ViewBag.SpecialistId = new SelectList(db.Specialists, "Id", "FirstName", training.SpecialistId);
                ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);

                var newTrainingStartDateWithTime = training.Date.AddHours(training.StartTime.Hour).AddMinutes(training.StartTime.Minute);
                var newTrainingFinishDateWithTime = newTrainingStartDateWithTime.AddMinutes(training.Duration);

                if (newTrainingStartDateWithTime > DateTime.Now)
                {
                    bool timeAlreadyBooked = false;

                    foreach (var oneTraining in db.Trainings)
                    {
                        var savedTrainingStartDateWithTime = oneTraining.Date.AddHours(oneTraining.StartTime.Hour).AddMinutes(oneTraining.StartTime.Minute);
                        var savedTrainingFinishDateWithTime = savedTrainingStartDateWithTime.AddMinutes(oneTraining.Duration);

                        if (newTrainingFinishDateWithTime > savedTrainingStartDateWithTime && newTrainingStartDateWithTime < savedTrainingFinishDateWithTime)
                        {
                            timeAlreadyBooked = true;
                            break;
                        }
                    }

                    if (!timeAlreadyBooked)
                    {
                        db.Entry(training).State = EntityState.Modified;
                        db.SaveChanges();
                        TempData["SavedMessage"] = "Treeningu muudatused on salvestatud";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Selleks ajaks on planeeritud teine treening. Palun vali uus treeningu kellaaeg.");
                        return View(training);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Treeningu algusaeg peab olema hiljem praegusest kellaajast.");
                    return View(training);
                }
            }
            return View(training);
        }

        // GET: Trainings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // POST: Trainings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Training training = db.Trainings.Find(id);
            db.Trainings.Remove(training);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
