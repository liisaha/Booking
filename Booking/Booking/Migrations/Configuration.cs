namespace Booking.Migrations
{
    using Booking.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Booking.Models.BookingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            //ContextKey = "Booking.Models.BookingContext";
        }

        protected override void Seed(Booking.Models.BookingContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //           

            ////Step 1 Create the user.
            //var passwordHasher = new PasswordHasher();
            //var user = new ApplicationUser();
            //user.UserName = "admin@test3.ee";
            //user.PasswordHash = passwordHasher.HashPassword("Admin1!");
            //user.SecurityStamp = Guid.NewGuid().ToString();
            //user.FirstName = "AdminEesnimi (Admin1!)";
            //user.LastName = "AdminPereknimi";
            //user.EmailConfirmed = false;
            //user.PhoneNumberConfirmed = false;
            //user.TwoFactorEnabled = false;
            //user.LockoutEnabled = true;
            //user.AccessFailedCount = 0;
            //user.Phone = "1234567";

            ////Step 2 Create and add the new Role.
            //var roleToChoose = new IdentityRole("AdminTest3");
            //context.Roles.AddOrUpdate(roleToChoose);

            ////Step 3 Create a role for a user
            //var role = new IdentityUserRole();
            //role.RoleId = roleToChoose.Id;
            //role.UserId = user.Id;

            ////Step 4 Add the role row and add the user to DB)
            //user.Roles.Add(role);
            //context.Users.AddOrUpdate(user);

        }
    }
}
