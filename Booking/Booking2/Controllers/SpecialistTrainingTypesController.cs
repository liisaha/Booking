﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    public class SpecialistTrainingTypesController : Controller
    {
        private BookingContext db = new BookingContext();

        // GET: SpecialistTrainingTypes
        public ActionResult Index()
        {
            var specialistTrainingTypes = db.SpecialistTrainingTypes.Include(s => s.Specialist).Include(s => s.TrainingType);
            return View(specialistTrainingTypes.ToList());
        }

        // GET: SpecialistTrainingTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialistTrainingType specialistTrainingType = db.SpecialistTrainingTypes.Find(id);
            if (specialistTrainingType == null)
            {
                return HttpNotFound();
            }
            return View(specialistTrainingType);
        }

        // GET: SpecialistTrainingTypes/Create
        public ActionResult Create()
        {
            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");
            return View();
        }

        // POST: SpecialistTrainingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SpecialistId,TrainingTypeId")] SpecialistTrainingType specialistTrainingType)
        {
            if (ModelState.IsValid)
            {
                db.SpecialistTrainingTypes.Add(specialistTrainingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", specialistTrainingType.SpecialistId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", specialistTrainingType.TrainingTypeId);
            return View(specialistTrainingType);
        }

        // GET: SpecialistTrainingTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialistTrainingType specialistTrainingType = db.SpecialistTrainingTypes.Find(id);
            if (specialistTrainingType == null)
            {
                return HttpNotFound();
            }
            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", specialistTrainingType.SpecialistId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", specialistTrainingType.TrainingTypeId);
            return View(specialistTrainingType);
        }

        // POST: SpecialistTrainingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SpecialistId,TrainingTypeId")] SpecialistTrainingType specialistTrainingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialistTrainingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", specialistTrainingType.SpecialistId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", specialistTrainingType.TrainingTypeId);
            return View(specialistTrainingType);
        }

        // GET: SpecialistTrainingTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialistTrainingType specialistTrainingType = db.SpecialistTrainingTypes.Find(id);
            if (specialistTrainingType == null)
            {
                return HttpNotFound();
            }
            return View(specialistTrainingType);
        }

        // POST: SpecialistTrainingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpecialistTrainingType specialistTrainingType = db.SpecialistTrainingTypes.Find(id);
            db.SpecialistTrainingTypes.Remove(specialistTrainingType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
