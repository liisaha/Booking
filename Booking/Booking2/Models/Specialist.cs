﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class Specialist
    {
        public int Id { get; set; }
        [Display(Name = "Eesnimi")]
        [Required(ErrorMessage = "Nimi ei või olla tühi.")]
        public string FirstName { get; set; }
        [Display(Name = "Perekonnanimi")]
        [Required(ErrorMessage = "Nimi ei või olla tühi.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Email ei või olla tühi.")]
        [EmailAddress(ErrorMessage= "Sisesta õige email")]
        public string Email { get; set; }
        [Display(Name = "Telefoninumber")]
        [Required(ErrorMessage = "Telefoninumber ei või olla tühi.")]
        public string Phone { get; set; } 

        public ICollection<Training> Training { get; set; }
        public ICollection<SpecialistTrainingType> SpecialistTrainingType { get; set; }

    }
}