﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class SpecialistTrainingType
    {
        public int Id { get; set; }

        //Foreign Key

        public int SpecialistId { get; set; }
        public Specialist Specialist { get; set; }
        //Foreign Key
        public int TrainingTypeId { get; set; }
        public TrainingType TrainingType { get; set; }

    }
}