﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    public class TrainingUsers2Controller : Controller
    {
        private Models.BookingContext db = new Models.BookingContext();

        // GET: TrainingUsers
        public ActionResult Index()
        {
            var trainingUsers = db.TrainingUsers;
            //var trainingUsers = db.TrainingUsers.Include(t => t.Client).Include(t => t.Training);
            return View(trainingUsers.ToList());
        }

        // GET: TrainingUsers/Create
        public ActionResult Create()
        {
            ViewBag.ClientId = new SelectList(db.Users, "Id", "Id");
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id");
            return View();
        }

        // POST: TrainingUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ClientId,TrainingId")] TrainingUser trainingClient)
        {
            if (ModelState.IsValid)
            {
                trainingClient.DateAdded = DateTime.Now;
                db.TrainingUsers.Add(trainingClient);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientId = new SelectList(db.Users, "Id", "Id", trainingClient.UserId);
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id", trainingClient.TrainingId);
            return View(trainingClient);
        }

        // GET: TrainingUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingUser trainingClient = db.TrainingUsers.Find(id);
            if (trainingClient == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientId = new SelectList(db.Users, "Id", "FirstName", trainingClient.UserId);
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id", trainingClient.TrainingId);
            return View(trainingClient);
        }

        // POST: TrainingUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DateAdded,ClientId,TrainingId")] TrainingUser trainingClient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trainingClient).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientId = new SelectList(db.Users, "Id", "FirstName", trainingClient.UserId);
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id", trainingClient.TrainingId);
            return View(trainingClient);
        }

        // GET: TrainingUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingUser trainingClient = db.TrainingUsers.Find(id);
            if (trainingClient == null)
            {
                return HttpNotFound();
            }
            return View(trainingClient);
        }

        // POST: TrainingUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrainingUser trainingClient = db.TrainingUsers.Find(id);
            db.TrainingUsers.Remove(trainingClient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
