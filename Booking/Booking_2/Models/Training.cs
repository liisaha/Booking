﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Booking.Models
{

    public class Training
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Kuupäev")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Kellaaeg")]
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }

        [Required]
        [Display(Name = "Kestvus")]
        public int Duration { get; set; }

        public ICollection<TrainingClient> TrainingClient { get; set; }

        //Foreign key
        public int SpecialistId { get; set; }
        public Specialist Specialist { get; set; }

        public int TrainingTypeId { get; set; }
        public TrainingType TrainingType { get; set; }

        [NotMapped]
        [Display(Name = "Nimi")]
        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", Specialist.FirstName, Specialist.LastName);
            }
        }

        [NotMapped]
        [Display(Name = "Vabu kohti")]
        public int FreeSpots { get; set; }
    }
}