﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    [Authorize(Roles = "Specialist, Admin")]
    public class TrainingTypesController : Controller
    {
        private Models.BookingContext db = new Models.BookingContext();

        // GET: TrainingTypes
        public ActionResult Index(string sortOrder)
        {
            ViewBag.TrainingTypeSortParm = String.IsNullOrEmpty(sortOrder) ? "trainingType_desc" : "";
            ViewBag.DescriptionSortParm = sortOrder == "Description" ? "description_desc" : "Description";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "price_desc" : "Price";
            ViewBag.MaxParticipantsSortParm = sortOrder == "MaxParticipants" ? "maxParticipants_desc" : "MaxParticipants";
            ViewBag.RoomSortParm = sortOrder == "Room" ? "room_desc" : "Room";
            var trainingTypes = from s in db.TrainingTypes
            select s;

            switch (sortOrder)
            {
                case "description_desc":
                    trainingTypes = trainingTypes.OrderByDescending(s => s.Description);
                    break;
                case "Description":
                    trainingTypes = trainingTypes.OrderBy(s => s.Description);
                    break;
                case "price_desc":
                    trainingTypes = trainingTypes.OrderByDescending(s => s.Price);
                    break;
                case "Price":
                    trainingTypes = trainingTypes.OrderBy(s => s.Price);
                    break;
                case "maxParticipants_desc":
                    trainingTypes = trainingTypes.OrderByDescending(s => s.MaxParticipants);
                    break;
                case "MaxParticipants":
                    trainingTypes = trainingTypes.OrderBy(s => s.MaxParticipants);
                    break;
                case "Room":
                    trainingTypes = trainingTypes.OrderBy(s => s.Room);
                    break;
                case "room_desc":
                    trainingTypes = trainingTypes.OrderByDescending(s => s.Room);
                    break;
                case "trainingType_desc":
                    trainingTypes = trainingTypes.OrderByDescending(s => s.Name);
                    break;
                default:
                    trainingTypes = trainingTypes.OrderBy(s => s.Name);
                    break;
            }
            return View(trainingTypes.ToList());
            //return View(db.TrainingTypes.ToList());
        }

        // GET: TrainingTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Price,MaxParticipants,Room")] TrainingType trainingType)
        {
            if (ModelState.IsValid)
            {
                var existingTrainingType = db.TrainingTypes.Where(x => x.Name == trainingType.Name).FirstOrDefault();

                if (existingTrainingType != null)
                {
                    ModelState.AddModelError("", "Sellise nimega treening on juba  registreeritud");
                    return View(trainingType);
                }

                db.TrainingTypes.Add(trainingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trainingType);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingType trainingType = db.TrainingTypes.Find(id);
            if (trainingType == null)
            {
                return HttpNotFound();
            }
            return View(trainingType);
        }

        // POST: TrainingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Price,MaxParticipants,Room")] TrainingType trainingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trainingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trainingType);
        }

        // GET: TrainingTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingType trainingType = db.TrainingTypes.Find(id);
            if (trainingType == null)
            {
                return HttpNotFound();
            }
            return View(trainingType);
        }

        // POST: TrainingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrainingType trainingType = db.TrainingTypes.Find(id);
            db.TrainingTypes.Remove(trainingType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
